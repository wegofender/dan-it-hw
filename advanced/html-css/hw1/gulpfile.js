let gulp = require('gulp');
let sass = require('gulp-sass');
let sync = require('browser-sync');
let autoprefixer = require('gulp-autoprefixer');
let cleanCss = require('gulp-clean-css');
let del = require('del');


gulp.task('clean', async function () {
    del.sync('dist')
});

gulp.task('sass', function () {
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass({outputStyle: 'expanded'}))
        .pipe(gulp.dest('app/css'))
        .pipe(sync.reload({stream: true}))
});

gulp.task('html', function () {
    return gulp.src('app/**/*.html')
        .pipe(sync.reload({stream: true}))
});

gulp.task('sync', function () {
    sync.init({
        server: {
            baseDir: 'app/'
        }
    })
});

gulp.task('watch', function () {
    gulp.watch('app/scss/**/*.scss', gulp.parallel('sass'));
    gulp.watch('app/**/*.html', gulp.parallel('html'))
});


gulp.task('export', async function () {
    gulp.src('app/**/*.html')
        .pipe(gulp.dest('dist'));
    gulp.src('app/css/*.css')
        .pipe(autoprefixer({cascade: true}))
        .pipe(cleanCss())
        .pipe(gulp.dest('dist/css'));
    gulp.src('app/js/*.js')
        .pipe(gulp.dest('dist/js'));
    gulp.src('app/img/*.*')
        .pipe(gulp.dest('dist/img'));
});

gulp.task('build', gulp.series('clean', 'export'));

gulp.task('default', gulp.parallel('sync', 'watch'));
class HamburgerException extends Error {
    constructor(message, param = {}) {
        super();
        this.name = 'HamburgerException';
        this.message = message;
        this.param = param.param || '';
    }
}

class Hamburger {
    static hamburgerProps = {
        SIZE_SMALL: {type: 'size', param: 'SIZE_SMALL', price: 50, calories: 20},
        SIZE_LARGE: {type: 'size', param: 'SIZE_LARGE', price: 100, calories: 40},
        STUFFING_CHEESE: {type: 'stuffing', param: 'STUFFING_CHEESE', price: 10, calories: 40},
        STUFFING_SALAD: {type: 'stuffing', param: 'STUFFING_SALAD', price: 20, calories: 40},
        STUFFING_POTATO: {type: 'stuffing', param: 'STUFFING_POTATO', price: 15, calories: 40},
        TOPPING_MAYO: {type: 'topping', param: 'TOPPING_MAYO', price: 15, calories: 0},
        TOPPING_SPICE: {type: 'topping', param: 'TOPPING_SPICE', price: 20, calories: 5}
    };

    constructor(size, stuffing) {

        if (!size) {
            throw new HamburgerException('no size given')
        }
        if (!stuffing) {
            throw new HamburgerException('no stuffing given')
        }
        // const {hamburgerProps} = this;
        if (size.type !== 'size') {
            throw new HamburgerException('invalid size', size)
        }
        if (stuffing.type !== 'stuffing') {
            throw new HamburgerException('invalid stuffing', stuffing)
        }

        this.size = size;
        this.stuffing = stuffing;
        this.topping = [];
    }

    set addTopping(topping) {
        if (!topping|| topping.type !== 'topping') {
            throw new HamburgerException('invalid topping', topping)
        } else if (this.topping.includes(topping)) {
            throw new HamburgerException('duplicate topping', topping)
        } else {
            this.topping.push(topping)
        }
    }

    set removeTopping(topping) {

        if (!this.topping.includes(topping)) {
            throw new HamburgerException('no such topping', topping)
        } else {
            this.topping = this.topping.filter(currentTopping => currentTopping !== topping)
        }
    }

    get getToppings() {
        return this.topping
    };

    get getSize() {
        return this.size
    };

    get getStuffing() {
        return this.stuffing
    };

    get price() {
        let price = this.size.price + this.stuffing.price;

        this.topping.forEach(topping => {
            price += topping.price
        });

        return price
    }

    get calories() {
        let calories = this.size.calories + this.stuffing.calories;

        this.topping.forEach(topping => {
            calories += topping.calories
        });

        return calories
    }
}


let hamburger = null; // declare hamburger variable in external scope for using try...catch

try {
    hamburger = new Hamburger(Hamburger.hamburgerProps.SIZE_LARGE, Hamburger.hamburgerProps.STUFFING_CHEESE);
    hamburger.addTopping = Hamburger.hamburgerProps.TOPPING_MAYO;
    hamburger.removeTopping = Hamburger.hamburgerProps.TOPPING_MAYO;

} catch (err) {
    console.error(`${err.name}: ${err.message} '${err.param || ''}'`)
}
console.log(hamburger);
// console.log(hamburger.getToppings);
// console.log(hamburger.getStuffing);
// console.log(hamburger.getSize);
// console.log(hamburger.price);
// console.log(hamburger.calories);
// console.log(hamburger);


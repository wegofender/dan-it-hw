class Card {
    static dragSrc = null;

    constructor(name) {
        this.card = document.createElement('div');
        this.card.classList.add('cards-item');
        this.card.setAttribute('draggable', 'true');
        this.card.innerText = name;
        this.addDragAndDrop(this.card)
    }

    renderCard(card, container) {
        container.append(card)
    }

    addDragAndDrop(card) {
        card.addEventListener('dragstart', this.handleDragStart);
        card.addEventListener('dragenter', this.handleDragEnter);
        card.addEventListener('dragover', this.handleDragOver);
        card.addEventListener('dragleave', this.handleDragLeave);
        card.addEventListener('drop', this.handleDrop);
        card.addEventListener('dragend', this.handleDragEnd);
    }

    handleDragStart(e) {
        Card.dragSrc = e.target;
        e.target.style.opacity = '0.4';
        e.dataTransfer.effectAllowed = 'move';

        e.dataTransfer.setData('text/html', e.target.innerHTML);
    }

    handleDragOver(e) {
        e.preventDefault();

        e.dataTransfer.dropEffect = 'move';
    }

    handleDragEnter(e) {
        e.target.classList.add('over');
    }

    handleDragLeave(e) {
        e.target.classList.remove('over');
    }

    handleDrop(e) {
        e.stopPropagation();
        console.log(e.target);

        Card.dragSrc.innerHTML = e.target.innerHTML;
        e.target.innerHTML = e.dataTransfer.getData('text/html');
    }

    handleDragEnd(e) {
        e.target.style.opacity = '1';
        document.querySelector('.over').classList.remove('over')
    }
}

class Column {

    static columns = [];

    constructor(name) {
        this.name = name;
        this.cards = [];
        this.card = new Card();
        this.renderColumn();
        this.container = document.getElementById(`cards-list-${this.name}`)
    }

    renderColumn() {
        const columnContainer = document.getElementById('columns');
        const column = document.createElement('div');
        const columnHeader = document.createElement('div');
        const columnHeading = document.createElement('h2');
        const sortButton = document.createElement('button');
        const cardList = document.createElement('div');
        const addCardForm = document.createElement('form');

        column.classList.add('app-column');
        columnHeader.classList.add('column-header');
        columnHeading.classList.add('column-heading');
        columnHeading.innerText = this.name;
        sortButton.classList.add('btn-sort');
        sortButton.innerText = 'Sort';
        sortButton.id = `sort-button-${this.name}`;
        cardList.classList.add('cards-list');
        cardList.id = `cards-list-${this.name}`;
        addCardForm.classList.add('add-card');
        addCardForm.id = 'add-card';

        columnHeader.append(columnHeading, sortButton);
        column.append(columnHeader, cardList, addCardForm);

        addCardForm.innerHTML = `<input id="card-name-${this.name}" type="text" class="column-input" placeholder="Card name">
                                    <button id="add-card-btn-${this.name}" class="btn btn-submit">
                                        <i class="fa fa-plus"></i>
                                    </button>`;

        columnContainer.append(column);
        this.addListeners()
    }

    addListeners() {
        document.getElementById(`sort-button-${this.name}`).addEventListener('click', () => {
            this.sortCards();
        });

        document.getElementById(`add-card-btn-${this.name}`).addEventListener('click', (e) => {
            this.addCard(e);
        });
    }

    addCard(e) {
        e.preventDefault();
        const nameInput = document.getElementById(`card-name-${this.name}`);
        const cardName = nameInput.value;
        if (cardName) {
            const newCard = new Card(cardName, this.name);
            this.cards.push(newCard);
            newCard.renderCard(newCard.card, this.container);
            nameInput.value = '';
        }
    }

    renderCards() {
        this.container.innerHTML = '';

        this.cards.forEach(item => {
            this.card.renderCard(item.card, this.container);
        })
    }

    sortCards() {
        this.cards.sort((a, b) => a.card.innerHTML > b.card.innerHTML ? 1 : -1);
        this.renderCards();
    }
}

class CreateColumn {

    constructor() {
        this.createButton = document.getElementById('column-add');
        this.columnNameInput = document.getElementById('column-name');
        this.addListeners();
    }

    addListeners() {
        this.createButton.addEventListener('click', this.addColumn.bind(this));
        this.columnNameInput.addEventListener('input', this.checkValidity.bind(this));
    }

    addColumn(e) {
        e.preventDefault();

        const columnName = this.columnNameInput.value;

        if (!this.checkValidity()) {
            Column.columns.push(columnName);

            new Column(columnName);
            this.columnNameInput.classList.remove('ok', 'error');
        }
    }

    checkValidity() {
        if (Column.columns.includes(this.columnNameInput.value)) {
            this.columnNameInput.classList.remove('ok');
            this.columnNameInput.classList.add('error');
        } else {
            this.columnNameInput.classList.remove('error');
            this.columnNameInput.classList.add('ok')
        }

        return Column.columns.includes(this.columnNameInput.value)
    }
}

new CreateColumn();

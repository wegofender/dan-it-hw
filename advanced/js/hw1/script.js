function HamburgerException(message, param) {
    this.name = 'HamburgerException';
    this.message = message;
    this.param = param;
}

HamburgerException.prototype = Object.create(TypeError.prototype);


function Hamburger(size, stuffing) {
    if (!size) {
        throw new HamburgerException('no size given')
    }
    if (!stuffing) {
        throw new HamburgerException('no stuffing given')
    }
    if (size.type !== 'size') {
        throw new HamburgerException('invalid size', size.param)
    }
    if (stuffing.type !== 'stuffing') {
        throw new HamburgerException('invalid stuffing', stuffing.param)
    }

    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];

}

Hamburger.SIZE_SMALL = {type: 'size', param: 'SIZE_SMALL', price: 50, calories: 20};
Hamburger.SIZE_LARGE = {type: 'size', param: 'SIZE_LARGE', price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {type: 'stuffing', param: 'STUFFING_CHEESE', price: 10, calories: 40};
Hamburger.STUFFING_SALAD = {type: 'stuffing', param: 'STUFFING_SALAD', price: 20, calories: 40};
Hamburger.STUFFING_POTATO = {type: 'stuffing', param: 'STUFFING_POTATO', price: 15, calories: 40};
Hamburger.TOPPING_MAYO = {type: 'topping', param: 'TOPPING_MAYO', price: 15, calories: 0};
Hamburger.TOPPING_SPICE = {type: 'topping', param: 'TOPPING_SPICE', price: 20, calories: 5};

Hamburger.prototype.addTopping = function (topping) {
    if (!topping || topping.type !== 'topping') {
        throw new HamburgerException('invalid topping', topping.param)
    } if (this.topping.includes(topping)) {
        throw new HamburgerException('duplicate topping', topping.param)
    } else {
        this.topping.push(topping)

    }
};

Hamburger.prototype.removeTopping = function (topping) {
    if (!this.topping.includes(topping)) {
        throw new HamburgerException('no such topping', topping)
    } else {
        this.topping = this.topping.filter(currentTopping => currentTopping !== topping);
    }
};

Hamburger.prototype.getToppings = function () {
    return this.topping
};

Hamburger.prototype.getSize = function () {
    return this.size
};

Hamburger.prototype.getStuffing = function () {
    return this.stuffing
};

Hamburger.prototype.calculatePrice = function () {
    var topping = this.topping;
    var price = this.size.price + this.stuffing.price;

    topping.forEach(topping => {
        price += topping.price
    });

    return price
};

Hamburger.prototype.calculateCalories = function () {
    var topping = this.topping;
    var calories = this.size.calories + this.stuffing.calories;

    topping.forEach(topping => {
        calories += topping.calories
    });

    return calories
};

function errLogger(err) {
    console.error(err.name + ': ' + err.message + ' \'' + (err.param || '') + '\'');
}

try {
    var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
} catch (err) {
    errLogger(err)
}



try {
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
} catch (err) {
    errLogger(err)
}

try {
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
} catch (err) {
    errLogger(err)
}

try {
    hamburger.removeTopping(Hamburger.TOPPING_MAYO);
} catch (err) {
    errLogger(err)
}

console.log(hamburger.getSize());
console.log(hamburger.getStuffing());
console.log(hamburger.getToppings());
console.log(hamburger.calculatePrice());
console.log(hamburger.calculateCalories());


console.log(hamburger);

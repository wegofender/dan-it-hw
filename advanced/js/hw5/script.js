const container = document.querySelector('.container');

function sendRequest() {
  const xhr = new XMLHttpRequest();
  const url = 'https://swapi.co/api/films/';

  xhr.open('GET', url);
  xhr.responseType = 'json';
  xhr.send();
  xhr.onload = () => {
    console.log(xhr.response);
    xhr.response.results.forEach((movie, cardId) => {
      const movieCard = document.createElement('div');
      const name = document.createElement('h3');
      const id = document.createElement('span');
      const crawls = document.createElement('p');
      const loader = document.createElement('div');

      movieCard.id = cardId;
      loader.innerHTML = `<div class="lds-ring"><div></div><div></div><div></div><div></div></div>`;

      name.innerText = movie.title;
      id.innerText = movie.episode_id;
      crawls.innerText = movie.opening_crawl;

      movieCard.append(name, id, crawls, loader);
      container.append(movieCard);

      const charsList = document.createElement('ul');
      const characters = movie.characters;
      const charNames = [];
      console.log(characters);
      characters.forEach(charUrl => {

        const charXhr = new XMLHttpRequest();
        charXhr.open('GET', charUrl);
        charXhr.responseType = 'json';
        charXhr.send();

        charXhr.onload = function () {
          charNames.push(charXhr.response.name);
          if (charNames.length === characters.length) {
            console.log('a');
            charsList.innerHTML = charNames.map(name => {
              return `
                     <li>
                        ${name}
                     </li>
                                    `
            }).join('');

            const currentMovie = document.getElementById(cardId);
            currentMovie.querySelector('.lds-ring').remove();
            currentMovie.append(charsList)
          }
        };
      });
    });
  }
}

sendRequest();



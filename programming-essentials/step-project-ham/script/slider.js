const $slider = $('.review-slider');
const timeoutCount = 2000;
const $reviewName = $('.review-name');
const $reviewPosition = $('.review-position');
const $reviewQuote = $('.review-quote');
const $reviewAvatar = $('.review-avatar');
const sliderContent = [...$('.slider-content:not(:last-child)').children()];
changeData(getData());

let sliderInterval = setInterval(nextSlide, timeoutCount);

$slider.on('click', (e) => {
    clearInterval(sliderInterval);

    $(e.target).is('.slider-avatar') ? toggleSlide(e) :
        $(e.target).is('.btn-next') ? nextSlide() :
            $(e.target).is('.btn-prev') ? prevSlide() : 0;

    sliderInterval = setInterval(nextSlide, timeoutCount);

});

function nextSlide() {
    const $nextSlide = $('.slider-avatar_active').next('.slider-avatar');
    if ($nextSlide.length) {
        $nextSlide
            .addClass('slider-avatar_active')
            .siblings()
            .removeClass('slider-avatar_active');
        changeData(getData());
    } else {
        $slider
            .find('.slider-avatar:first')
            .addClass('slider-avatar_active')
            .siblings()
            .removeClass('slider-avatar_active');
        changeData(getData());
    }
}

function prevSlide() {
    const $prevSlide = $('.slider-avatar_active').prev('.slider-avatar');
    if ($prevSlide.length) {
        $prevSlide
            .siblings()
            .removeClass('slider-avatar_active');
        $prevSlide
            .addClass('slider-avatar_active');
        changeData(getData());
    } else {
        $slider
            .find('.slider-avatar:last')
            .addClass('slider-avatar_active')
            .siblings()
            .removeClass('slider-avatar_active');
        changeData(getData());
    }
}

function toggleSlide(e) {
    $(e.target)
        .addClass('slider-avatar_active')
        .siblings()
        .removeClass('slider-avatar_active');
    changeData(getData())
}

function getData() {
    const $currentSlide = $slider.find('.slider-avatar_active');
    return {
        name: $currentSlide.data('name'),
        position: $currentSlide.data('position'),
        quote: $currentSlide.data('quote'),
        photo: $currentSlide.attr('src')
    }
}

function changeData({name, position, quote, photo}) {
    $reviewName.text(name);
    $reviewPosition.text(position);
    $reviewQuote.text(quote);
    $reviewAvatar
        .fadeTo(50, 0.3, () => {
            $('.review-avatar').attr('src', photo)
        })
        .fadeTo(50, 1)
}


// знаю, что можно было проще, но решил побаловаться )
const trackChange = new MutationObserver(mutations => {
    mutations.forEach(mutation => {
        const trackedNode = $(mutation.target);
        if (mutation.removedNodes.length) {
            trackedNode.fadeOut(1)
        }
        if (mutation.addedNodes.length) {
            trackedNode.fadeIn(130)
        }
    })
});


// небольшая оптимизация, MutationObserver работает, только если cлайдер появляется в вьюпорте )
window.addEventListener('scroll', () => {
    if (isInViewPort($('.slider-content')[0])) {
        sliderContent.forEach(node => {
            trackChange.observe(node, {
                characterData: false,
                attributes: false,
                childList: true,
                subtree: false
            });
        })
    } else trackChange.disconnect()
});



const $serviceTabs = $('.services-list');
const $serviceContent = $('.services-content');

$serviceContent.children(':not(:first-child)').hide();

$serviceTabs.delegate('.service-title', 'click', function() {
    const tabIndex = $(this).index();
    $(this).addClass('service-title_active').siblings().removeClass('service-title_active')

    $($serviceContent.children()[tabIndex])
        .show()
        .siblings()
        .hide();

    console.log($(this).siblings());

})
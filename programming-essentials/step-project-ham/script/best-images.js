const $grid = $('.grid');
const $firstGridImages = $('.grid-item:nth-child(-n+8)');
const $imagesBtn = $('#images-btn');

$grid.masonry({
    itemSelector: '.grid-item',
    columnWidth: 370,
    gutter: 17,
    stagger: 200
});

$(window).on('scroll', () => {
    if (isInViewPort($grid[0])) {
        $(window).off();
        $firstGridImages.removeAttr('hidden');
        $grid.append($firstGridImages).masonry('appended', $firstGridImages);
        $imagesBtn.show().delay(1800)
    }
});

$imagesBtn.on('click', () => {
    $imagesBtn.remove();
    $('#grid-loader').show();
    setTimeout(() => {
        $('#grid-loader').remove();
        addImages()
    }, 2000)
});

function addImages() {
    const $additionalImages = $('.grid-item:hidden');
    $additionalImages.removeAttr('hidden');
    $grid.append($additionalImages).masonry('appended', $additionalImages)
    // $imagesBtn.remove()
}

createOverlay(['.grid-item:not(.grid-multiple)', '.inner-grid-item']);


function createOverlay(classNames) {

    classNames.forEach(className => {
        document.querySelectorAll(className).forEach(elem => {
            const overlay = document.createElement('div');
            overlay.classList.add('grid-overlay');
            overlay.innerHTML = `<div class="grid-buttons">
                              <button class="grid-btn">
                                  <i class="fa fa-search"></i>
                              </button>
                              <button class="grid-btn">
                                  <i class="fa fa-arrows"></i>
                              </button>
                          </div>`;
            elem.append(overlay)
        })
    })

}

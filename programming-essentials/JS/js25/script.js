const slides = document.querySelectorAll('.slide');
const slider = document.querySelector('.slider-wrapper');
const container = document.querySelector('.slider-container');
const nextBtn = document.querySelector('.arrow-right');
const prevBtn = document.querySelector('.arrow-left');
const size = parseFloat(window.getComputedStyle(container).width);
const buttonSet = createButtonSet();
let counter = 1;

createClones();
highlightButton();
generateSky()

nextBtn.addEventListener('click', () => {
    if (counter >= [...slider.children].length - 1) {
        return
    }
    counter++;
    slider.style.transition = 'all .3s';
    slider.style.transform = `translateX(${-size * counter}px)`;
    highlightButton()
});

prevBtn.addEventListener('click', () => {
    if (counter <= 0) {
        return
    }
    slider.style.transition = 'all .3s'
    counter--;
    slider.style.transform = `translateX(${(-size * counter)}px)`
    highlightButton()
});

buttonSet.addEventListener('click', (e) => {
    [...buttonSet.children].forEach(button => {
        button.classList.remove('active')
    })
    if (e.target.classList.contains('slide-link-item')) {
        e.target.classList.add('active')
        counter = [...buttonSet.children].indexOf(e.target) + 1
        slider.style.transition = 'all .3s'
        slider.style.transform = `translateX(${(-size * counter)}px)`
    }
})

slider.addEventListener('transitionend', () => {
    if (counter === [...slider.children].indexOf(slider.lastElementChild)) {
        counter = 1;
        slider.style.transition = 'none';
        slider.style.transform = `translateX(${-size * counter}px)`;
    }

    if (counter === [...slider.children].indexOf(slider.firstElementChild)) {
        counter = [...slider.children].length - 2;
        slider.style.transition = 'none';
        slider.style.transform = `translateX(${-size * counter}px)`;
    }
});

function createButtonSet() {
    const buttons = document.querySelector('.slider-links');
    for (let i = 0; i < slides.length; i++) {
        const button = document.createElement('span');
        button.classList.add('slide-link-item');
        buttons.append(button)
    }
    return buttons
}

function createClones() {
    const lastClone = slides[slides.length - 1].cloneNode(true);
    const firstClone = slides[0].cloneNode(true);
    slider.prepend(lastClone);
    slider.append(firstClone);
    slider.style.transform = `translateX(-${size}px)`
}

function highlightButton() {
    const localButtonSet = [...buttonSet.children];
    console.log(localButtonSet);
    console.log(localButtonSet[counter - 1]);
    localButtonSet.forEach(button => {
        button.classList.remove('active')
    });
    if (counter === [...slider.children].indexOf(slider.lastElementChild)) {
        localButtonSet[0].classList.add('active')
    } else if (counter === [...slider.children].indexOf(slider.firstElementChild)) {
        localButtonSet[localButtonSet.length - 1].classList.add('active')
    } else localButtonSet[counter - 1].classList.add('active')
}

function generateSky() {
    for (let i = 0; i < 400; i++){
        document.body.append(createStar(getCoords()))
    }
}

function createStar({x, y}) {
    const starSize = Math.round(Math.random() * 5)
    const star = document.createElement('span');
    star.classList.add('star');
    star.style.width = `${starSize}px`;
    star.style.height = `${starSize}px`;
    star.style.top = `${y}px`;
    star.style.left = `${x}px`;
    star.style.transform = `translate(${-starSize / 2}px ${-starSize / 2}px)`
    return star
}

function getCoords() {
    return {
        x: Math.round(Math.random() * document.body.clientWidth),
        y: Math.round(Math.random() * document.body.clientHeight)
    }
}


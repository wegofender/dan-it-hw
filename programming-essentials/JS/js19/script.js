let obj = {
    // count: 128,
    // adc: null,
    arr: [1, 2, 3],
    a1: {
        name: 'test',
        b1: {
            c: 1
        },
        b2: {
            c: 222
        },
        getName: function () {
            return this.name
        },
        b3: {
            c: {
                d: 33,
                e: 2.5,
                f: {
                    g: 9999,
                    h: {
                        i: {
                            j: 1001,
                            k: 'строка',
                            l: [{1: {sdf: 1}, name: 'dude'}, [1, 2]]
                        }
                    }
                }
            }
        }
    }
};

let clone = deepClone(obj);
console.group('FINAL');
console.log(clone);
console.groupEnd();

function deepClone(obj) {
    let clone;
    if (Array.isArray(obj)) {
        clone = []
    } else clone = {};
    for (let key in obj) {
        if (obj[key] !== null && typeof obj[key] === 'object') {
            clone[key] = deepClone(obj[key])
        } else clone[key] = obj[key]
    }
    return clone
}

// function dClone(el) {
//     const funcsObj = {
//         "Object": () => {
//             let clObj = {};
//             for(let prop in el) {
//                 clObj[prop] = dClone(el[prop]);
//             }
//             return clObj;
//         },
//         "Array": () => {
//             return el.map((i) => {
//                 return dClone(i);
//             });
//         }
//     };
//     if (el.constructor.name in funcsObj) {
//         return funcsObj[el.constructor.name]();
//     } else {
//         return el;
//     }
// }
//



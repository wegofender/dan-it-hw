const $menu = $('.tabs');
const $tabsContent = $('.tabs-content');
$menu.delegate('.tabs-title', 'click', function () {
    let clickedIndex = $(this).index();
    $(this).addClass('active').siblings().removeClass('active');
    $($tabsContent.children()[clickedIndex]).removeClass('hidden').siblings().addClass('hidden')
});


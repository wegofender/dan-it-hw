const images = document.querySelector('.images-wrapper');
const buttonSet = document.createElement('div');

const timerField = document.createElement('span');

const slideShowSpeed = 10000;

const imageList = [...images.children];
const second = 1000;
const minute = second * 60;

let timerCount;
let isActive = true; // boolean flag, points on state of slide show (false - idle; true - active)

// starting point of script execution
startTimer();

// button settings
const stopButton = document.createElement('button');
const continueButton = document.createElement('button');

stopButton.innerText = 'Прекратить';
continueButton.innerText = 'Возобновить показ';

buttonSet.classList.add('buttons');
stopButton.classList.add('btn', 'btn-stop');
continueButton.classList.add('btn', 'btn-continue');

buttonSet.append(stopButton, continueButton);
document.body.append(buttonSet);

stopButton.addEventListener('click', stopSlideShow);
continueButton.addEventListener('click', () => {
    if (!isActive){
        changeImage(); // без этой функции не будет менятся картинка по клику
        startTimer();
    }
    isActive = true
});


function stopSlideShow() {
    isActive = false;
    clearInterval(timerCount)
}

function changeImage() {
    let start = images.querySelector('img:not(.hidden)'); //4
    imageList[imageList.indexOf(start)]
        .classList
        .toggle('hidden');
    if (imageList[imageList.indexOf(start)]
        .nextElementSibling === null) {
        imageList[0].classList
            .toggle('hidden');
    } else {
        imageList[imageList.indexOf(start)]
            .nextElementSibling.classList
            .toggle('hidden')
    }
}

function startTimer() {

    let end = new Date();
    end.setSeconds(end.getSeconds() + slideShowSpeed / 1000);
    document.body.append(timerField);
    timerCount = setInterval(updateTimer, 1, end)
}

function updateTimer(end) {
    let now = new Date();
    let distance = end - now;
    if (distance < 0) {
        console.log(distance);
        clearInterval(timerCount);
        startTimer();
        changeImage();
        return
    }
    let minutes = Math.trunc((distance / minute));
    let seconds = Math.trunc((distance % minute) / second);
    let milliseconds = Math.trunc((distance % second) / 10);

    timerField.innerText = `${minutes}:${seconds}:${milliseconds}`
}










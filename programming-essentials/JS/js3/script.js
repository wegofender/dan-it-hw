// v1

let num1;
let num2;
let operation;

do {
    num1 = prompt('Math\nEnter first number:', num1);
    num2 = prompt('Math\nEnter second number:', num2);
} while (isNaN(+num1) || num1 === null || isNaN(+num2) || num2 === null);

do {
    operation = prompt('Math\nEnter your operation:');
} while (operation !== '+' && operation !== '-' && operation !== '/' && operation !== '*') ;


alert(`Math\nYour result: ${mathFunc(+num1, +num2, operation)}`);

function mathFunc(num1, num2, operation) {
    switch (operation) {
        case '+' :
            return num1 + num2;
        case '-' :
            return num1 - num2;
        case '/' :
            return num1 / num2;
        case '*' :
            return num1 * num2;
    }
}

// v2

// function mathFunc() {
//     let num1;
//     let num2;
//     let operation;
//
//     do {
//         num1 = prompt('Math\nEnter first number:', num1);
//         num2 = prompt('Math\nEnter second number:', num2);
//     } while (isNaN(+num1) || num1 === null || isNaN(+num2) || num2 === null);
//
//     do {
//         operation = prompt('Math\nEnter your operation:');
//     } while (operation !== '+' && operation !== '-' && operation !== '/' && operation !== '*');
//
//     switch (operation) {
//         case '+' :
//             return num1 + num2;
//         case '-' :
//             return num1 - num2;
//         case '/' :
//             return num1 / num2;
//         case '*' :
//             return num1 * num2;
//     }
// }


// alert(`Math\nYour result: ${mathFunc()}`);

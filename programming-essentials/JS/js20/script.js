// дедлайном считать 00:00 дня, который указан в переменной deadline. Работа начинается сегодня, в начале рабочего дня.
'use strict'


let today = new Date();
let deadline = new Date(2019, 9, 28);
let team = [4, 8, 15, 12, 9];
let backlog = [17, 13, 21, 3, 31, 19, 80];

function sprintPanning(team, backlog, deadline) {
    // считаем сумму стори поинтов в беклоке и дневную продуктивность команды
    let teamDailyProductivity = team.reduce((a, b) => a + b, 0);
    let backlogSum = backlog.reduce((a, b) => a + b, 0);

    // считаем дни до дедлайна, получив милисекунды, делимна количество милисекунд в сутках
    let daysToDeadline = Math.ceil((deadline - new Date()) / 8.64e+7);
    let workdaysToDeadline = daysToDeadline - weekendCount(deadline);   // отнимаем количество выходных

    // для простоты дальнейших расчетов находим "целый (int) беклог", который без остатка делится на продуктивность
    // команды и "остаточный (rest) беклог" который необходим для подсчета остатка часов, нужных для завершения всех
    // задач
    let intBacklog = backlogSum - (backlogSum % teamDailyProductivity);
    let restBacklog = backlogSum - intBacklog;

    // в цикле считаем количесво дней, необходимых для завершения задач
    let currentDay = today;
    let dayCount = 0;   // 4
    while (intBacklog > 0) {
        if (currentDay.getDay() !== 6 || currentDay.getDay() !== 0) { // проверка на выходной
            intBacklog -= teamDailyProductivity;
            dayCount++;
            console.log(dayCount);
        }
    }
    // определяем счетчик часов
    let hoursCount = Math.ceil(restBacklog * 8 / teamDailyProductivity);

    // проверка основного условия
    if (dayCount + hoursCount / 8 < workdaysToDeadline) {
        alert(`Все задачи будут успешно выполнены за ${Math.floor(workdaysToDeadline - (dayCount + hoursCount
            / 8))} дней и ${8 - hoursCount} часов до наступления дедлайна!`);
    } else alert(`Команде разработчиков придется потратить дополнительно` +
        ` ${(dayCount - workdaysToDeadline) * 8 + hoursCount} ` +
        `часов после дедлайна, чтобы выполнить все задачи в беклоге`);


    // блок тестов которые использовались для отлалки кода
    console.group('Tests');
    console.log(`Сегодня: ${today}`);
    console.log(`Дедлайн: ${deadline}`);
    console.log(`Рабочих дней до дедлайна: ${workdaysToDeadline}`);
    console.log(`Необходимо для выполнения беклога, ${dayCount} рабочих дня/ей и ${hoursCount} часа/ов`);
    console.log(`Backlog sum ${backlogSum}`);
    console.log(`Team power: ${teamDailyProductivity}`);
    console.log(`Full days to finish: ${intBacklog}`);
    console.groupEnd();
}
console.time();
sprintPanning(team, backlog, deadline);
console.timeEnd();


function weekendCount(deadline) {   //функция подсчета выходных на отрезке времени до дедлайна
    let currentDay = new Date();
    let count = 0;
    while (currentDay < deadline) {
        if (currentDay.getDay() === 6 || currentDay.getDay() === 0) {
            count++;
        }
        currentDay = new Date(currentDay.setDate(currentDay.getDate() + 1));
    }
    return count;
}




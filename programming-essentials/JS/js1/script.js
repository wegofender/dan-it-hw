let age = '';
let name = '';
do {
    name = prompt('What is your name?', name);
    age = prompt('Your age?', age);
} while (name === null || isNaN(age));

if (age < 18) {
    alert('You are not allowed to visit this website');
} else if (age > 18 && age <= 22) {
    let isUserSure = confirm('Are you sure you want to continue?');
    if (isUserSure) {
        alert('Welcome, ' + name);
    } else alert('You are not allowed to visit this website');
} else alert('Welcome, ' + name);

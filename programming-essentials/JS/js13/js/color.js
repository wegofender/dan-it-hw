const wrapper = document.querySelector('.site-wrapper');
const toggleButton = document.getElementById('toggle-theme');

if (!localStorage.colorTheme) {
    localStorage.setItem('colorTheme', 'light')
}
if (localStorage.getItem('colorTheme') === 'dark') {
    changeTheme();
}

toggleButton.addEventListener('click', () => {
    if (localStorage.getItem('colorTheme') === 'light') {
        changeTheme();
        localStorage['colorTheme'] = 'dark';
    } else if (localStorage.getItem('colorTheme') === 'dark') {
        changeTheme();
        localStorage['colorTheme'] = 'light';
    }
});

function changeTheme() {
    wrapper.classList.toggle('dark');
}

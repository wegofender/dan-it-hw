let apple = {
    name: 'apple',
    color: 'green'
};
let arr = [
    1,
    2,
    [2.1, 2.2, [45, 56]],
    8,
    9
];
// body.textContent = "<i>Hello World!</i>"

// Timer settings

const second = 1000;
const minute = second * 60;
const hour = minute * 60;
let end = new Date();
let timerDuration = 10;
end.setSeconds(end.getSeconds() + timerDuration);

// Creating basic structure

let container = document.createElement('div');
let timerField = document.createElement('span');
document.body.append(container);

// container.innerHTML = generateList(arr); // Generating list
container.append(generateList(arr));
container.append(timerField);

// let timerCount = setInterval(timer, 1);
// setTimeout(() => {
//     clearInterval(timerCount)
//     clearPage(container);
// }, timerDuration * 1000);


function generateList(arr) {
    const ul = document.createElement('ul');
    arr.map(elem => {
        const li = document.createElement('li');
        if (Array.isArray(elem)) {
            ul.children[ul.children.length - 1].append(generateList(elem));
        } else ul.innerHTML += `<li>${elem}</li>`
        li.innerText = elem;
    });
    return ul;
}


// function createList(array) {
//     let list = document.createElement('ul');
//     array.map( ((value) => {
//         if (typeof value === 'object') {
//             list.append(createList(value));
//         } else {
//             list.innerHTML += `<li>${value}</li>`;
//         }
//     }) );
//     return list
// }


// function printToUl(array) {
//     document.body.append(createList(array));
// }
// printToUl(['hello', 'world', 'Kiev', ['sub-list1', 34, ['sub-list2', 34], 'Kharkiv', 'Odessa', 'Lviv']);
// printToUl(['1', '2', '3', 'sea', 'user', 23]);
//



function timer() {
    let now = new Date();
    let distance = end - now;
    let hours = Math.trunc(distance / hour);
    let minutes = Math.trunc((distance % hour) / minute);
    let seconds = Math.trunc((distance % minute) / second);
    let milliseconds = Math.trunc((distance % second) / 10);

    timerField.innerText = `${hours}:${minutes}:${seconds}:${milliseconds}`
}

function clearPage(element) {
    element.remove()
}


// function generateList(arr) {

// function parseObject(obj) { // for nested objects
//     return `<ul>${Object.entries(obj).map(([key, value]) => {
//         if (Array.isArray(value)) {
//             return generateList(value)
//         }
//         if (typeof value === 'object') {
//             return `${parseObject(value)}`
//         }
//         return `<li>${key}: ${value}</li>`
//     }).join('')}</ul>`
// }
//     return `<ul>${arr.map(elem => {
//
//         if (Array.isArray(elem)) {
//             return `${generateList(elem)}`
//         } else return (`<li>${elem}</li>`);
//     }).join('')}</ul>`
// }
let vehicles = [
    {
        name: 'Toyota',
        description: 'car',
        contentType: [
            {name: 'collection'},
            {year: 2002},
        ],
        locales: {
            name: 'Тойота',
            description: 'ru_RU'
        }
    },
    {
        name: 'Mercedes',
        description: 'van',
        contentType: [
            {name: 'collection'},
            {year: 2007},
        ],
        locales: {
            name: 'Мерседес',
            description: 'ru_RU'
        }
    },
    {
        name: 'BMW',
        description: 'sport',
        contentType: [
            {name: 'racing'},
            {year: 2018},
        ],
        specs: {
            features: [
                {color: 'red'},
                {drive: 'RWD'}
            ],
            performance: {
                hp: 374,
                topSpeed: 250
            }
        },
        locales: {
            nameLocale: 'БМВ',
            description: 'ru_RU'
        },
    }
];


function filterCollection(collection, values, searchAllMatches, ...fields) {
    const keywords = values.split(' ');
    fields = fields.map(field => field.split('.'));

    let result = [];
    if (searchAllMatches) {
        result = collection.filter(obj => {
            return keywords.every(keyword => {
                return deepSearch(obj, keyword, fields)
            })
        })
    } else {
        result = collection.filter(obj => {
            return keywords.some(keyword => {
                return deepSearch(obj, keyword, fields)
            })
        })
    }
    return result
}

function deepSearch(obj, keyword, localFields) {
    if (!localFields || !localFields.length) {
        return Object.entries(obj).some(([key, value]) => {
            if (typeof value !== 'object') {
                return compare(value, keyword)
            } else if (Array.isArray(value)) {
                let flag = value.some(elem => {
                    return deepSearch(elem, keyword)
                });
                if (flag) {
                    return flag
                }
            } else return deepSearch(value, keyword)
        });
    } else return Object.entries(obj)
        .filter(([key]) => localFields.some(field => field[0] === key))
        .some(([key, value]) => {
            if (typeof value !== 'object') {
                return compare(value, keyword)
            } else if (Array.isArray(value)) {
                const tempField = localFields
                    .filter(field => field[0] === key)
                    .map(field => field.slice(1));
                let flag = value.some(elem => {
                    return deepSearch(elem, keyword, tempField);
                });
                if (flag) {
                    return flag
                }
            } else {
                const tempField = localFields
                    .filter(field => field[0] === key)
                    .map(field => field.slice(1));
                return deepSearch(value, keyword, tempField)
            }
        })
}

function compare(value, keyword) {
    console.log(value, keyword);
    const res = value.toString().toLowerCase() === keyword.toString().toLowerCase();
    console.log(res);
    return res
}

let answer;
// some test
// answer = filterCollection(vehicles, '374', true, 'specs.performance.hp');
// answer = filterCollection(vehicles, 'Mercedes ТойоТа', false, 'name', 'locales.name');
// answer = filterCollection(vehicles, 'Mercedes 374', true, 'name', 'specs.performance.hp');
answer = filterCollection(vehicles, 'Mercedes Toyota', false, 'name');
// answer = filterCollection(vehicles, 'БМВ', true);


console.log(answer);
//




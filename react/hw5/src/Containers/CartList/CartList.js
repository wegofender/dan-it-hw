import React from 'react';
import { NavLink } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect'
import { reset } from 'redux-form'
import CartItem from '../../components/CartItem/CartItem';
import Modal from '../../components/Modal/Modal'
import Button from '../../components/UI/Button/Button'
import CartForm from '../../components/CartForm/CartForm'
import { toggleModal } from '../../store/actions/modalActions';
import { setProduct, setProductCount, clearCart } from '../../store/actions/productActions'

const CartList = () => {
  const productsSelector = state => state.products.products;
  const cartProductsSelector = createSelector(productsSelector,
    products => products.filter(prod => prod.isInCart)
  );

  const dispatch = useDispatch();
  const cartProducts = useSelector(cartProductsSelector);
  const isModal = useSelector(state => state.modal.cartModal);
  const currentProductId = useSelector(state => state.modal.currentProductId)

  const handleModalSubmit = (id) => {
    dispatch(setProduct('cart', id));
    dispatch(toggleModal());
  };

  const handleCartFormSubmit = values => {
    const cartData = {
      order: cartProducts, 
      user: values,
    }

    dispatch(reset('cart'));
    dispatch(clearCart())

    console.log('Cart data in JSON', JSON.stringify(cartData))
  }

  return (
    <section>
      <div className="container">
        <h2 className={'section-heading'}>
          Корзина
          </h2>
        <div className="cart-wrapper">
          <div className="cart-container">

            {cartProducts.length
              ? cartProducts.map((product, index) => {
                return (
                  <CartItem
                    key={product.id}
                    product={product}
                    onRemove={() => dispatch(toggleModal(product.id))}
                    onChange={count => dispatch(setProductCount(product.id, count))}
                  />
                )
              })
              : <span className="cart-placeholder">
                  Пусто :(
                    <NavLink
                      exact
                      className="cart-placeholder__link"
                      to="/">
                      Исправим ?
                    </NavLink>
                </span>}
          </div>
          {cartProducts.length && (
            <div className="cart-container" >
              <CartForm onSubmit={handleCartFormSubmit} />
            </div>
          )}
        </div>
        {
          isModal &&
          <Modal
            header={'Подтверждение'}
            text={`Вы уверены, что хотите удалить товар из корзины?`}
            closeButton={true}
            closeHandler={() => dispatch(toggleModal())}
            type={'submit'}
            actions={[<Button
              autoFocus
              key={1}
              text={'Да'}
              onClick={() => handleModalSubmit(currentProductId)}
              backgroundColor={'red'}
            />]}
          />}
      </div>

    </section>
  )
};

export default CartList;
import React, { useEffect } from 'react';
import {Route, Switch} from 'react-router-dom'
import { useDispatch } from 'react-redux'
import ProductsList from './Containers/ProductsList/ProductsList';
import CartList from './Containers/CartList/CartList';
import FavoritesList from './Containers/FavoritesList/FavoritesList';
import Header from './components/Header/Header';
import { fetchProducts } from './store/actions/productActions'


const App = () => {

  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(fetchProducts())
  }, [dispatch])

  return (
    <>
      <Header/>

      <Switch>
        <Route exact path="/">
          <ProductsList/>
        </Route>
        <Route path="/favorites">
          <FavoritesList/>
        </Route>
        <Route path="/cart">
          <CartList/>
        </Route>


      </Switch>
    </>
  )
}

export default App;

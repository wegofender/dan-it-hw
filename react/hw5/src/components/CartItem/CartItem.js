import React, { memo } from 'react';

const CartItem = ({ product, onRemove, onChange }) => {
  const {name, image_url, count, price} = product;

  return (

    <div className="cart-item">
      <span 
       className="remove-btn"
       onClick={onRemove}
       >&times;</span>
      <div className="cart-item__content">
        <img src={image_url} alt="Trash Blonde" className="cart-item__image"/>
        <p className="cart-item__name">
          {name}
        </p>
      </div>
      <div className="cart-item-count">
        <div className="cart-item-count__controls">
          <button onClick={() => onChange(count - 1)} className="cart-btn">-</button>
          <input
            className={'cart-item-count__input'}
            type="text"
            value={count}
            onChange={(e) => onChange(+e.target.value)}
          />
          <button onClick={() => onChange(count + 1)} className="cart-btn">+</button>
        </div>
        <p className={'cart-item-count__price'}>
          {price * count} UAH
        </p>
      </div>

    </div>

  )
};

export default memo(CartItem)
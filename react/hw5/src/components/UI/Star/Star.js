import React, { memo, useMemo } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Star = ({ onClick, isFavorite }) => {

  const iconType = isFavorite ? 'fas' : 'far';
  const icon = useMemo(() => <FontAwesomeIcon className={'star'} size={'lg'} icon={[iconType, 'star']} />, [iconType])
  return (
    <div onClick={onClick}>
      { icon }
    </div>
  );
}

export default memo(Star);
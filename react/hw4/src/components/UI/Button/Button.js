import React, { memo } from 'react';
import PropTypes from 'prop-types'


const Button = ({ onClick, disabled, backgroundColor, text }) => {

  return (
    <button
      className={'button'}
      style={{ 'backgroundColor': backgroundColor }}
      onClick={onClick}
      disabled={disabled}
    >
      {text}
    </button>
  )
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string
};

export default memo(Button);

export const TOGGLE_MODAL = 'TOGGLE_MODAL';

export const toggleModal = (id = null )=> ({
  type: TOGGLE_MODAL,
  payload: id 
});


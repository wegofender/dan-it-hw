import axios from 'axios';

export const GET_PRODUCTS = 'GET_PRODUCTS';
export const GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';
export const GET_PRODUCTS_ERROR = 'GET_PRODUCTS_ERROR';
export const SET_PRODUCT_TO_FAVORITES = 'SET_PRODUCT_TO_FAVORITES';
export const SET_PRODUCT_TO_CART = 'SET_PRODUCT_TO_CART';
export const SET_PRODUCT_COUNT = 'SET_PRODUCT_COUNT';

const getProducts = () => ({
  type: GET_PRODUCTS,
});

const getProductsSuccess = (products) => ({
  type: GET_PRODUCTS_SUCCESS,
  payload: products,
});

const getProductsError = () => ({
  type: GET_PRODUCTS_ERROR,
});

const setProductToCart = id => ({
  type: SET_PRODUCT_TO_CART,
  payload: id,
});

const setProductToFavorites = id => ({
  type: SET_PRODUCT_TO_FAVORITES,
  payload: id,
});

export const setProductCount = (id, count) => ({
  type: SET_PRODUCT_COUNT,
  payload: {id, count},
})

export const fetchProducts =  () => async dispatch => {
  dispatch(getProducts());
  const localProducts = JSON.parse(localStorage.getItem('localProducts'));

  return axios.get('./data.json', {
    headers: {
      'Accept': 'application/json'
    }
  }).then(res => {
    if (res.status !== 200) {
      dispatch(getProductsError())
    } else {
      const productsWithCartAndFavorites = res.data.map(product => {
        return {
          ...product,
          // if localProducuts contains item with same id => set to value of this item 
          // else => set to false or 0
          isInCart: localProducts[product.id]?.cart || false,
          isFavorite: localProducts[product.id]?.favorite || false,
          count: localProducts[product.id]?.count || 0
        }
      });
      dispatch(getProductsSuccess(productsWithCartAndFavorites));
    }
  });
};

export const setProduct = (target, id) => dispatch => {
  switch (target) {
    case 'cart' : 
      dispatch(setProductToCart(id));
      break;
    case 'favorite' :
      dispatch(setProductToFavorites(id));
      break;
    default : 
      break; 
  };
};

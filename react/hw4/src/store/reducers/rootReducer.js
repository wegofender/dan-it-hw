import { combineReducers } from 'redux';
import { modalReducer } from './modalReducer'
import { productsReducer } from './productsReducer'
import { cartReducer } from './cartReducer';


export const rootReducer = combineReducers({
    modal: modalReducer, 
    products: productsReducer, 
    cart: cartReducer,
  })
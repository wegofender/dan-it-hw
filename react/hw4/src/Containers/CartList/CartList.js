import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect'
import CartItem from '../../components/CartItem/CartItem';
import Modal from '../../components/Modal/Modal'
import Button from '../../components/UI/Button/Button'
import { toggleModal } from '../../store/actions/modalActions';
import { setProduct, setProductCount } from '../../store/actions/productActions'

const CartList = () => {
  const productsSelector = state => state.products.products;
  const cartProductsSelector = createSelector(productsSelector,
    products => products.filter(prod => prod.isInCart)
  );

  const dispatch = useDispatch();
  const cartProducts = useSelector(cartProductsSelector);
  const isModal = useSelector(state => state.modal.cartModal);
  const currentProductId = useSelector(state => state.modal.currentProductId)

  const handleModalSubmit = (id) => {
    dispatch(setProduct('cart', id));
    dispatch(toggleModal());
  };

  return (
    <section>
      <div className="container">
        <div className="cart-wrapper">
          <h2 className={'section-heading'}>
            Корзина
          </h2>
          <div className="cart-list">

            {cartProducts.length
              ? cartProducts.map((product, index) => {
                return (
                  <CartItem
                    key={product.id}
                    product={product}
                    onRemove={() => dispatch(toggleModal(product.id))}
                    onChange={count => dispatch(setProductCount(product.id, count))}
                  />
                )
              })
              : null}
          </div>
        </div>
        {
          isModal &&
          <Modal
            header={'Подтверждение'}
            text={`Вы уверены, что хотите удалить товар из корзины?`}
            closeButton={true}
            closeHandler={() => dispatch(toggleModal())}
            type={'submit'}
            actions={[<Button
              key={1}
              text={'Да'}
              onClick={() => handleModalSubmit(currentProductId)}
              backgroundColor={'red'}
            />]}
          />}
      </div>

    </section>
  )
};

export default CartList;
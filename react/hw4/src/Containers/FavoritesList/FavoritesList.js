import React from 'react';
import List from '../../components/List/List';
import { useSelector } from 'react-redux'
import { createSelector } from 'reselect'

const productsSelector = state => state.products.products;
const favoriteProsuctsSelector = createSelector(
  productsSelector, 
  products => products.filter(prod => prod.isFavorite)
)

const FavoritesList = () => {

  const favoriteProductsList = useSelector(favoriteProsuctsSelector);

  return (
    <div className='container'>
      <h1 className="section-heading">Избранное</h1>
      <List
        products={favoriteProductsList}
      />
    </div>
  )
};

export default FavoritesList
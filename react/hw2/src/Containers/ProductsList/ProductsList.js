import React, {Component} from 'react';
import withProducts from '../../hoc/withProducts/withProducts';
import {getProducts} from '../../hoc/withProducts/products-service';
import Product from '../../components/Product/Product';

class ProductsList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: []
    };
  }

  render() {
    return (
      <div className={'products-list'}>
        {this.props.products.map((product, index) => {
          return (
            <Product
              key={product.id}
              name={product.name}
              id={product.id}
              price={product.price}
              src={product.image_url}
              isInCart={product.isInCart}
              isFavorite={product.isFavorite}
              setToCart={this.props.onModalOpen}
              setToFavorites={this.props.setProductToFavorites}
            />
          )
        })}
      </div>
    );
  }
}


export default withProducts(ProductsList, getProducts);
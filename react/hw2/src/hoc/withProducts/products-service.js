export const getProducts = async () => {
  return fetch('./data.json', {
    headers: {
      'Accept': 'application/json'
    }
  })
    .then(response => response.json())
    .then(products => {
      if (!localStorage.getItem('cart') || !localStorage.getItem('favorites')) {
        localStorage.setItem('favorites', JSON.stringify([]));
        localStorage.setItem('cart', JSON.stringify([]));
      }

      const productsWithCartAndFavorites = products.map(product => {
        const cartProducts = JSON.parse(localStorage.getItem('cart'));
        const favoriteProducts = JSON.parse(localStorage.getItem('favorites'));

        return {
          ...product,
          isInCart: cartProducts.some(item => item.id === product.id),
          isFavorite: favoriteProducts.some(item => item.id === product.id)
        }
      });
      return productsWithCartAndFavorites;
    })

};

export const getFavorites = () => {
  return JSON.parse(localStorage.getItem('favorites'))
};
import React, {Component, Fragment} from 'react';
import Modal from '../../components/Modal/Modal';
import Button from '../../components/UI/Button/Button';

const withProducts = (WrappedComponent, getProducts) => {
  return class ProductsWrapper extends Component {
    constructor(props) {
      super(props);

      this.state = {
        products: [],
        isModal: false,
        currentProductId: null
      }
    }

    async componentDidMount() {
      const products = await getProducts();
      this.setState(
        {products}
      )
    }

    setProduct(id, local, target) {
      const products = [...this.state.products];
      const localProducts = JSON.parse(localStorage.getItem(local));
      const currentProduct = products.find(product => product.id === id);

      currentProduct[target] = !currentProduct[target];

      if (!localProducts.some(product => product.id === id)) {
        localProducts.push(currentProduct)
      } else {
        const idx = localProducts.indexOf(localProducts.find(product => product.id === id));
        localProducts.splice(idx, 1);
      }

      this.setState({
        products
      });

      localStorage.setItem(local, JSON.stringify(localProducts))
    }

    setProductToCart(id) {
      this.handleModalToggle();
      this.setProduct.call(this, id, 'cart', 'isInCart')
    }

    setProductToFavorites(id) {
      this.setProduct.call(this, id, 'favorites', 'isFavorite');
    };

    handleModalToggle(id) {
      this.setState({
        currentProductId: id,
        isModal: !this.state.isModal
      });
    }

    render() {
      const currentProduct = this.state.products.find(product => product.id === this.state.currentProductId);

      return (
        <Fragment>
          <WrappedComponent
            onModalOpen={this.handleModalToggle.bind(this)}
            setProductToFavorites={this.setProductToFavorites.bind(this)}
            products={this.state.products}
          />
          {this.state.isModal &&
          <Modal
            header={'Подтверждение'}
            text={`Вы уверены, что хотите добавить товар ${currentProduct.name} в корзину?`}
            closeButton={true}
            closeHandler={this.handleModalToggle.bind(this)}
            type={'submit'}
            actions={[<Button
              key={1}
              text={'ОК'}
              onClick={this.setProductToCart.bind(this, this.state.currentProductId)}
              backgroundColor={'red'}
            />]}
          />}
        </Fragment>
      )
    }
  }
};

export default withProducts;
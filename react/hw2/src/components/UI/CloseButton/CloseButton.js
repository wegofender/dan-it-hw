import React, {Component} from 'react'
import PropTypes from 'prop-types'


class CloseButton extends Component {
  render() {
    return (
      <div className={'close-btn'} onClick={this.props.onCLick}>
        &times;
      </div>
    );
  }
}

CloseButton.propTypes = {
  onClick: PropTypes.func
};

export default CloseButton;
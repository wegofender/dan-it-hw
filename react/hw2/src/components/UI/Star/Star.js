import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Star extends Component {
  render() {
    const iconType = this.props.isFavorite ? 'fas' : 'far';

    return (
      <div onClick={this.props.onClick}>
        <FontAwesomeIcon className={'star'} size={'lg'} icon={[iconType, 'star']}/>
      </div>
    );
  }
}

export default Star;
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Button from '../UI/Button/Button';
import Star from '../UI/Star/Star';

class Product extends Component {

  render() {

    return (
      <div className={'product'}>
        <div className="product__body">
          <img
            className={'product__image'}
            src={this.props.src}
            alt="Trash Blonde"
          />
          <p className="product__name">
            {this.props.name}
          </p>
        </div>
        <div className="product__footer">
          <Button
            text={this.props.isInCart ? 'В корзине!' : 'Купить'}
            backgroundColor={'red'}
            onClick={() => this.props.setToCart(this.props.id)}
            disabled={!!this.props.isInCart}
          />
          <span className="product__price">
            {this.props.price} UAH
          </span>

          <Star
            onClick={() => this.props.setToFavorites(this.props.id)}
            isFavorite={this.props.isFavorite}
          />
        </div>
      </div>
    );
  }
}

Product.propTypes = {
  src: PropTypes.string,
  name: PropTypes.string,
  price: PropTypes.number,
  isFavorite: PropTypes.bool,
  isInCart: PropTypes.bool,
  setToFavorite: PropTypes.func,
  setToCart: PropTypes.func
};

export default Product;
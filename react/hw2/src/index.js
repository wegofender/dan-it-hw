import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import './App.scss'
import App from './App';
import {fas} from '@fortawesome/free-solid-svg-icons'
import {library} from '@fortawesome/fontawesome-svg-core'
import {far} from '@fortawesome/free-regular-svg-icons'

library.add(fas, far);

ReactDOM.render(<App />, document.getElementById('root'));

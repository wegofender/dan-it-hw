import React, {Component} from 'react';
import ProductsList from './Containers/ProductsList/ProductsList';

class App extends Component {
  render() {
    return (
      <ProductsList/>
    )
  }
}

export default App;

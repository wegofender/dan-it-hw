export const getProducts = async () => {

  if (!localStorage.getItem('cart') || !localStorage.getItem('favorites')) {
    localStorage.setItem('favorites', JSON.stringify([]));
    localStorage.setItem('cart', JSON.stringify([]));
  };

  const cartProducts = JSON.parse(localStorage.getItem('cart'));
  const favoriteProducts = JSON.parse(localStorage.getItem('favorites'));

  return fetch('./data.json', {
    headers: {
      'Accept': 'application/json'
      }
    })
    .then(response => response.json())
    .then(products => {
      const productsWithCartAndFavorites = products.map(product => {
        return {
          ...product,
          isInCart: cartProducts.some(item => item.id === product.id),
          isFavorite: favoriteProducts.some(item => item.id === product.id)
        }
      });
      return productsWithCartAndFavorites;
    })
};
import React, {Fragment} from 'react';
import {Route, Switch} from 'react-router-dom'
import ProductsList from './Containers/ProductsList/ProductsList';
import CartList from './Containers/CartList/CartList';
import FavoritesList from './Containers/FavoritesList/FavoritesList';
import Header from './components/Header/Header';


const App = () => {

  return (
    <Fragment>

      <Header/>

      <Switch>
        <Route exact path="/">
          <ProductsList/>
        </Route>
        <Route path="/favorites">
          <FavoritesList/>
        </Route>
        <Route path="/cart">
          <CartList/>
        </Route>


      </Switch>
    </Fragment>
  )
}

export default App;

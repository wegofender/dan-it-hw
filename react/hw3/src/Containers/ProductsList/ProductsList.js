import React from 'react';
import List from '../../components/List/List';
import { getProducts } from '../../services/productsService';

const ProductsList = () => {

  return (
    <div className="container">
      <h1 className="section-heading">Товары</h1>
      <List getItems={getProducts} />
    </div>
  );
};

export default ProductsList;


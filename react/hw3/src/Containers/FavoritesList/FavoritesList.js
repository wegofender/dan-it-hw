import React from 'react';
import List from '../../components/List/List';
import { getProducts } from '../../services/productsService';



const FavoritesList = props => {

  return (
    <div className='container'>
      <h1 className="section-heading">Избранное</h1>
      <List
        type={'favorites'}
        getItems={getProducts}
      />
    </div>
  )
};

export default FavoritesList
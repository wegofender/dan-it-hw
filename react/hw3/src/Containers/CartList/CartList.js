import React, { useEffect, useState} from 'react';
import CartItem from '../../components/CartItem/CartItem';
import Modal from '../../components/Modal/Modal'
import Button from '../../components/UI/Button/Button'


const CartList = () => {
  const [cartProducts, setCartProducts] = useState([]);
  const [modal, setModal] = useState(false)
  const [currentProductId, setCurrentProductId] = useState(null);


  useEffect(() => {
    const products = JSON.parse(localStorage.getItem('cart'))
    setCartProducts(products)
  }, []);

  const removeProductFromCartHandler = id => {
    const localProducts = cartProducts.filter(product => product.id !== id)

    setCartProducts(localProducts)
    setModal(prevState => !prevState);
    localStorage.setItem('cart', JSON.stringify(localProducts))
  };

  const itemCountChangeHandler = id => value => {
    const localProducts = [...cartProducts];
    const currentProduct = localProducts.find(product => product.id === id);
    currentProduct.count = value > 0 ? value : 1;

    setCartProducts(localProducts);
    localStorage.setItem('cart', JSON.stringify(localProducts))
  }

  const handleModalToggle = (id) => {
    setModal(prevState => !prevState);
    id && setCurrentProductId(id);
  };
    return (
    <section>
      <div className="container">
        <div className="cart-wrapper">
          <h2 className={'section-heading'}>
            Корзина
          </h2>
          <div className="cart-list">

            {cartProducts
              ? cartProducts.map((product, index) => {
                return (
                  <CartItem
                    key={product.id}
                    product={product}
                    onRemove={() => handleModalToggle(product.id)}
                    onChange={itemCountChangeHandler(product.id)}
                  />
                )
              })
              : null}
          </div>
        </div>
        {
          modal &&
          <Modal
            header={'Подтверждение'}
            text={`Вы уверены, что хотите удалить товар из корзины?`}
            closeButton={true}
            closeHandler={handleModalToggle}
            type={'submit'}
            actions={[<Button
              key={1}
              text={'Да'}
              onClick={() => removeProductFromCartHandler(currentProductId)}
              backgroundColor={'red'}
            />]}
          />}
      </div>

    </section>
  )
};

export default CartList;
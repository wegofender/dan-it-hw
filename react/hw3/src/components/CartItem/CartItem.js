import React, { memo } from 'react';

const CartItem = props => {
  const {name, image_url, count, price} = props.product;

  return (

    <div className="cart-item">
      <span 
       className="remove-btn"
       onClick={props.onRemove}
       >&times;</span>
      <div className="cart-item__content">
        <img src={image_url} alt="Trash Blonde" className="cart-item__image"/>
        <p className="cart-item__name">
          {name}
        </p>
      </div>
      <div className="cart-item-count">
        <div className="cart-item-count__controls">
          <button onClick={() => props.onChange(count - 1)} className="cart-btn">-</button>
          <input
            className={'cart-item-count__input'}
            type="text"
            value={count}
            onChange={(e) => props.onChange(+e.target.value)}
          />
          <button onClick={() => props.onChange(count + 1)} className="cart-btn">+</button>
        </div>
        <p className={'cart-item-count__price'}>
          {price * count} UAH
        </p>
      </div>

    </div>

  )
};

export default memo(CartItem)
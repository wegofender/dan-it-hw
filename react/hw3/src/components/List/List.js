import React, { Fragment, useEffect, useState } from 'react';
import Product from '../Product/Product';
import Modal from '../Modal/Modal';
import Button from '../UI/Button/Button';

import './List.scss'

const List = ({type, getItems}) => {
  const [products, setProducts] = useState([]);
  const [isModal, setModal] = useState(false);
  const [currentProductId, setCurrentProductId] = useState(null);

  useEffect(() => {
    (async () => {
      let products = await getItems();
      if (type === 'favorites') {
        products = products.filter(product => product.isFavorite)
      }
      setProducts(products);
    })()

  }, [currentProductId, type, getItems]);

  const setProduct = (id, local, target) => {
    const currentProducts = [...products];
    const currentProduct = currentProducts.find(product => product.id === id);
    if (local === 'cart') {
      currentProduct.count = 1
      currentProduct.sum = currentProduct.price
    }
    const localProducts = JSON.parse(localStorage.getItem(local));

    currentProduct[target] = !currentProduct[target];

    if (!localProducts.some(product => product.id === id)) {
      localProducts.push(currentProduct)
    } else {
      const idx = localProducts.indexOf(localProducts.find(product => product.id === id));
      localProducts.splice(idx, 1);
    }

    setProducts(currentProducts);
    localStorage.setItem(local, JSON.stringify(localProducts))
  };

  const setProductToCart = (id) => {
    handleModalToggle(id);
    setProduct(id, 'cart', 'isInCart')
  };

  const setProductToFavorites = (id) => {
    setCurrentProductId(id);

    setProduct(id, 'favorites', 'isFavorite');
  };

  const handleModalToggle = (id) => {
    setModal(prevState => !prevState);
    setCurrentProductId(id);
  };


  const currentProduct = products.find(product => product.id === currentProductId);


  return (
    <Fragment>
      <div className='list'>
        {products.map((product, index) => {
          return (
            <Product
              key={product.id}
              name={product.name}
              id={product.id}
              price={product.price}
              src={product.image_url}
              isInCart={product.isInCart}
              isFavorite={product.isFavorite}
              setToCart={() => handleModalToggle(product.id)}
              setToFavorites={() => setProductToFavorites(product.id)}
            />)
        })}
      </div>
      {isModal &&
        <Modal
          header={'Подтверждение'}
          text={`Вы уверены, что хотите добавить товар ${currentProduct.name} в корзину?`}
          closeButton={true}
          closeHandler={handleModalToggle}
          type={'submit'}
          actions={[<Button
            key={1}
            text={'ОК'}
            onClick={() => setProductToCart(currentProductId)}
            backgroundColor={'red'}
          />]}
        />}

    </Fragment>
  )
};

export default List;
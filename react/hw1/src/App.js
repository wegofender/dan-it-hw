import React, {Component} from 'react';
import Button from './components/UI/Button/Button'
import Modal from './components/Modal/Modal';


class App extends Component {
  state = {
    isModal: false,
    currentModal: null
  };

  deleteModalOpenHandler() {
    this.setState({
      isModal: true,
      currentModal: 'deleteModal'
    })
  }

  successModalOpenHandler() {
    this.setState({
      isModal: true,
      currentModal: 'successModal'
    })
  }

  modalCloseHandler() {
    this.setState({
      isModal: !this.state.isModal,
      currentModal: null
    })
  }

  render() {
    const modals = {
      deleteModal: {
        type: 'delete',
        header: 'Do you want to delete this file?',
        text: `Once you delete this file, it won’t be possible to undo this action.
               Are you sure you want to delete it?`,
        closeButton: true,
        actions: [
          <Button
            key={0}
            text={'OK'}
            backgroundColor={'#00000030'}
            onClick={this.modalCloseHandler.bind(this)}
          />,
          <Button
            key={1}
            text={'Cancel'}
            backgroundColor={'#00000030'}
            onClick={this.modalCloseHandler.bind(this)}
          />
        ]
      },
      successModal: {
        type: 'success',
        header: 'Very Good!',
        text: `Congratulations my friend! You successfully passed the exam!`,
        closeButton: true,
        actions: [
          <Button
            key={0}
            text={'OK'}
            backgroundColor={'#00000030'}
            onClick={this.modalCloseHandler.bind(this)}
          />,
        ]}
    }
    const config = modals[this.state.currentModal]

    const modal = this.state.isModal &&
      <Modal
        type={config.type}
        header={config.header}
        text={config.text}
        actions={config.actions}
        closeButton={config.closeButton}
        closeHandler={this.modalCloseHandler.bind(this)}
      />;
    
    return (
      <div className={'buttons'}>
        <Button
          text={'Success'}
          backgroundColor={'green'}
          onClick={this.successModalOpenHandler.bind(this)}
        />
        <Button
          text={'Delete'}
          backgroundColor={'red'}
          onClick={this.deleteModalOpenHandler.bind(this)}
        />
        {modal}
      </div>
    )
  }
}

export default App;

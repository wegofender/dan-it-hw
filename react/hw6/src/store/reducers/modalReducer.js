import { TOGGLE_MODAL } from "../actions/modalActions"

const initialState = {
  cartModal: false,
  currentProductId: null,
}

export const modalReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case TOGGLE_MODAL:
      return {
        ...state,
        cartModal: !state.cartModal,
        currentProductId: payload,
      }
  default:
    return state
  }
}

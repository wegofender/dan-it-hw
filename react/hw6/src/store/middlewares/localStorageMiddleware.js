import {
  SET_PRODUCT_TO_CART,
  SET_PRODUCT_TO_FAVORITES,
  SET_PRODUCT_COUNT,
  CLEAR_CART
} from '../actions/productActions'

const checkLocalProductLength = (products, id) => !Object.keys(products[id]).length > 0 && delete products[id]

export const localStorageMiddleware = store => next => action => {
  const { type, payload } = action

  if (type.includes('redux-form')) {
    return next(action)
  }

  !localStorage.getItem('localProducts') &&
    localStorage.setItem('localProducts', JSON.stringify({}))

  const localProducts = JSON.parse(localStorage.getItem('localProducts'))
  const products = store.getState().products.products.reduce((accum, prod) => {
    accum[prod.id] = prod;
    return accum
  }, {})

  switch (type) {
    case SET_PRODUCT_TO_CART:
      if (products[action.payload].isInCart) {
        delete localProducts[payload].count;
        delete localProducts[payload].cart;
      } else {
        localProducts[payload] = {
          ...localProducts[payload],
          cart: true,
          count: 1
        }
      }
      checkLocalProductLength(localProducts, payload)
      break;
    case SET_PRODUCT_TO_FAVORITES:
      if (products[action.payload].isFavorite) {
        delete localProducts[payload].favorite;
      } else {
        localProducts[payload] = {
          ...localProducts[payload],
          favorite: true
        }
      }
      checkLocalProductLength(localProducts, payload)
      break;
    case SET_PRODUCT_COUNT:
      localProducts[payload.id].count = payload.count > 0 ? payload.count : 1;
      break;
    case CLEAR_CART : 
      Object.keys(localProducts).forEach(key => {
        delete localProducts[key].count;
        delete localProducts[key].cart;
        checkLocalProductLength(localProducts, key)
      })
    default:
      break;
  }

  localStorage.setItem('localProducts', JSON.stringify(localProducts))
  return next(action)
}
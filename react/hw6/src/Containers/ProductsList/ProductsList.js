import React from 'react';
import List from '../../components/List/List';
import { useSelector } from 'react-redux';


const ProductsList = () => {

  const products = useSelector(state => state.products.products)

  return (
    <div className="container">
      <h1 className="section-heading">Товары</h1>
      <List
        products={products}
      />
    </div>
  );
};

export default ProductsList;


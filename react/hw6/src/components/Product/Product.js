import React, { memo } from 'react'
import PropTypes from 'prop-types'
import Button from '../UI/Button/Button';
import Star from '../UI/Star/Star';

const Product = props => {

  return (
    <div className={'product'}>
      <div className="product__body">
        <img
          className={'product__image'}
          src={props.src}
          alt="Trash Blonde"
        />
        <p className="product__name">
          {props.name}
        </p>
      </div>
      <div className="product__footer">
        <Button
          text={props.isInCart ? 'В корзине!' : 'Купить'}
          backgroundColor={'red'}
          onClick={() => props.setToCart(props.id)}
          disabled={!!props.isInCart}
        />
        <span className="product__price">
            {props.price} UAH
          </span>

        <Star
          onClick={() => props.setToFavorites(props.id)}
          isFavorite={props.isFavorite}
        />
      </div>
    </div>
  );
};

Product.propTypes = {
  src: PropTypes.string,
  name: PropTypes.string,
  price: PropTypes.number,
  isFavorite: PropTypes.bool,
  isInCart: PropTypes.bool,
  setToFavorite: PropTypes.func,
  setToCart: PropTypes.func
};

export default memo(Product);
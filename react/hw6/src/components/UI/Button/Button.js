import React, { memo } from 'react';
import PropTypes from 'prop-types'


const Button = ({ onClick, disabled, backgroundColor, text, autoFocus }) => {

  return (
    <button
      className={'button'}
      style={{ 'backgroundColor': backgroundColor }}
      onClick={onClick}
      disabled={disabled}
      autoFocus={autoFocus}
    >
      {text}
    </button>
  )
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string
};

export default memo(Button);

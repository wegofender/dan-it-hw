import React from 'react';
import { shallow, mount } from 'enzyme';
import ReactDOM from 'react-dom'
import Modal from './Modal';
import Button from '../UI/Button/Button';
import CloseButton from '../UI/CloseButton/CloseButton'


describe('Modal component', () => {
  
  it('Should have appropriate text', () => {
    const modal = shallow( 
      <Modal
        header={'Подтверждение'}
        text={`Вы уверены, что хотите добавить товар в корзину?`}
      />)

    expect(modal.find('.modal-header__text').text()).toEqual('Подтверждение');
    expect(modal.find('.modal-content').text()).toEqual(`Вы уверены, что хотите добавить товар в корзину?`);
  })

  it('Should have close button', () => {
    const modal = shallow( 
      <Modal
        closeButton={true}
      />)

      expect(modal.find(CloseButton)).toHaveLength(1);
  })

  it('should close on backdrop click', () => {
    const mockCallback = jest.fn();
    const modal = shallow(<Modal closeHandler={mockCallback}/>)
    const backdrop = modal.find('.backdrop');
    backdrop.simulate('click');

    expect(mockCallback.mock.calls.length).toBe(1)
  })

  it('shold close on close button click', () => {
    const mockCallback = jest.fn();
    const modal = shallow(<Modal closeButton={true} closeHandler={mockCallback}/>)
    const button = modal.find(CloseButton)
    button.dive().find('.close-btn').simulate('click');

    expect(mockCallback.mock.calls.length).toBe(1)
  })

  it('submit button should be focused', () => {
    const mockCallback = jest.fn();
    const modal = shallow(
      <Modal
        actions={
          [<Button
          autoFocus
          key={1}
          text={'ОК'}
          onClick={mockCallback}
          backgroundColor={'red'}
          />]
        }
      />
    )
    const button = modal.find(Button).dive().find('button');

    expect(button.prop('autoFocus')).toBe(true);
  })
})
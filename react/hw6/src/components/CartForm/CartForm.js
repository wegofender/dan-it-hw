import React from 'react'
import { Field, reduxForm } from 'redux-form'
import Input from '../UI/Input/Input'
import Button from '../UI/Button/Button'


const required = value => value ? undefined : 'Это обязаьельное поле';
const number = value => value && isNaN(Number(value)) ? 'Возраст должен быть числом' : undefined;
const string = value => value && !isNaN(Number(value)) ? 'Введите корректное значение' : undefined;
const phone = value => (
  value && !/^\+?3?8?(0[5-9][0-9]\d{7})$/i.test(value) 
    ?'Неверный номер телефона' 
    : undefined 
)

const CartForm = props => {
  console.log('form props', props)
  const { handleSubmit } = props
  return (
    <form onSubmit={handleSubmit}>
          <Field
            name="firstName"
            component={Input}
            type="text"
            label="Имя"
            validate={[required, string]}
          />
          <Field
            name="lastName"
            component={Input}
            type="text"
            label="Фамилия"
            validate={[required, string]}
          />
          <Field
            name="age"
            component={Input}
            type="text"
            label="Возраст"
            validate={[required, number]}
          />
          <Field
            name="address"
            component={Input}
            type="text"
            label="Адрес доставки"
            validate={[required, string]}
          />
          <Field
            name="phone"
            component={Input}
            type="text"
            label="Мобильный телефон"
            validate={[required, phone]}
          />
      <Button backgroundColor="red" text="Подтвердить покупки" />
    </form>
  )
}

export default reduxForm({
  form: 'cart',
})(CartForm)

// export default CartForm
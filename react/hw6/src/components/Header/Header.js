import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => (
  <header className="header">
    <nav className="header-nav">
      <ul className="menu">
        <li className="menu__item">
          <NavLink
            exact
            className="menu__link"
            activeClassName="menu__link--active"
            to="/">
            Товары
            </NavLink>
        </li>
        <li className="menu__item">
          <NavLink
            className="menu__link"
            activeClassName="menu__link--active"
            to="/favorites">
            Избранное
            </NavLink>
        </li>
        <li className="menu__item">
          <NavLink
            className="menu__link"
            activeClassName="menu__link--active"
            to="/cart">
            Корзина
            </NavLink>
        </li>
      </ul>
    </nav>
  </header>

)

export default Header